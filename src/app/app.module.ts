import { Toast } from '@ionic-native/toast';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MediaCapture } from '@ionic-native/media-capture';
import { AngularFireModule } from "angularfire2";
import { MyApp } from './app.component';
import { Media, MediaObject } from '@ionic-native/media';

let config = {
  apiKey: "AIzaSyCYsttDrV4dlK7wqDAtmQA3eu3XsQ_OEj0",
  authDomain: "demospeack.firebaseapp.com",
  databaseURL: "https://demospeack.firebaseio.com",
  projectId: "demospeack",
  storageBucket: "demospeack.appspot.com",
  messagingSenderId: "1089962936153"
};
@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    Toast,
    Media,
    SplashScreen,
    MediaCapture,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }

import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { MediaCapture, MediaFile, CaptureError, CaptureAudioOptions, CaptureImageOptions } from '@ionic-native/media-capture';
import { Media, MediaObject } from '@ionic-native/media';
import { Toast } from '@ionic-native/toast';

@IonicPage({})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public title: string = "Speack Chat Test"
  public temporal_speack: any;
  public play: boolean = false;
  constructor(public navCtrl: NavController,
    private media: Media,
    private toast: Toast,
    private mediaCapture: MediaCapture) {
  }


  spaeack() {
    this.toast.show(`Capturando audio`, '5000', 'center').subscribe(
      toast => {
        console.log(toast);
      }
    );
    let options: CaptureAudioOptions = { limit: 1, duration: 10 };
    this.mediaCapture.captureAudio(options)
      .then(
      (data: MediaFile[]) => {
        this.toast.show(`Captura de audio finalizada`, '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
        console.log(data)
        this.play = true;
        this.temporal_speack = data[0].fullPath;
        this.playSound();

      },
      (err: CaptureError) => {
        this.toast.show(`Se ha presentado un error al capturar el audio, verifica que poseas instalada una aplicación para capturar audio.`, '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
        console.error(err)
      }
      );

    // let options: CaptureImageOptions = { limit: 3 };
    // this.mediaCapture.captureImage(options)
    //   .then(
    //   (data: MediaFile[]) => console.log(data),
    //   (err: CaptureError) => console.error(err)
    //   );
  }

  playSound() {
    this.toast.show(`Reproduciendo audio capturado`, '5000', 'center').subscribe(
      toast => {
        console.log(toast);
      }
    );
    const file: MediaObject = this.media.create(this.temporal_speack);
    file.onSuccess.subscribe(() => {
      console.log('Action is successful')
      this.toast.show(`Reproducción finalizada`, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });

    file.onError.subscribe(error => {
      console.log('Error!', error)
      this.toast.show(`Se ha presentado un error al reproducir el audio ${error}`, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });

    file.play();
  }

}